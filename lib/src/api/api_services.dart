import 'package:flutter_app_todo/src/model/tasks.dart';
import 'package:http/http.dart' show Client;

class ApiServices {
  final String baseUrl = "http://192.168.43.160:3000";
  Client client = Client();

  Future<List<Tasks>> getTasks() async {
    final response = await client.get("$baseUrl/tasks");
    if (response.body.isNotEmpty) {
      return tasksFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<bool> createTasks(Tasks data) async {
    final response = await client.post(
      "$baseUrl/tasks",
      headers: {"content-type": "application/json"},
      body: tasksToJson(data),
    );
    if (response.body.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateTasks(Tasks data) async {
    final response = await client.put(
      "$baseUrl/tasks/${data.id}",
      headers: {"content-type": "application/json"},
      body: tasksToJson(data),
    );
    if (response.body.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> deleteTasks(int id) async {
    final response = await client.delete(
      "$baseUrl/tasks/$id",
      headers: {"content-type": "application/json"},
    );
    if (response.body.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }
}
