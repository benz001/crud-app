import 'package:flutter/material.dart';
import 'package:flutter_app_todo/src/ui/formadd/form_add_task.dart';
import 'package:flutter_app_todo/src/ui/home/main_home.dart';


GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {

  void insert() async {
    var result = await Navigator.push(
      _scaffoldState.currentContext,
      MaterialPageRoute(builder: (BuildContext context) {
        return FormAddTask();
      }),
    );
    if (result != null) {
      setState(() {
       
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.orange,
        accentColor: Colors.orangeAccent,
      ),
      home: Scaffold(
        key: _scaffoldState,
        appBar: AppBar(
          title: Text(
            "To Do List App",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
        body: MainHome(),
        floatingActionButton:FloatingActionButton(
          onPressed: insert,
          tooltip: 'Increment',
          child: Icon(Icons.add),
        )),
    );
  }
}
