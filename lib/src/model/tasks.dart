import 'dart:convert';

class Tasks {
  int id;
  String title;
  String task;
  String deadline;
  String status;

  Tasks({this.id = 0, this.title, this.task, this.deadline, this.status});

  factory Tasks.fromJson(Map<String, dynamic> map) {
    return Tasks(
        id: map["id"], 
        title: map["title"], 
        task: map["task"], 
        deadline: map["deadline"], 
        status: map["status"]
        );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id, 
      "title": title, 
      "task": task, 
      "deadline": deadline,
      "status": status
      };
  }

  @override
  String toString() {
    return 'Tasks{id: $id, title: $title, task: $task, deadline: $deadline, status: $status}';
  }

}

List<Tasks> tasksFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Tasks>.from(data.map((item) => Tasks.fromJson(item)));
}

String tasksToJson(Tasks data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
