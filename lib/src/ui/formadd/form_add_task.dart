import 'package:flutter/material.dart';
import 'package:flutter_app_todo/src/api/api_services.dart';
import 'package:flutter_app_todo/src/model/tasks.dart';
import 'package:intl/intl.dart';


final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

class FormAddTask extends StatefulWidget {
  Tasks tasks;

  FormAddTask({this.tasks});

  @override
  _FormAddTaskState createState() => _FormAddTaskState();
}

class _FormAddTaskState extends State<FormAddTask> {
  bool _isLoading = false;
  ApiServices _apiServices = ApiServices();
  bool _isFieldTitleValid;
  bool _isFieldTaskValid;
  bool _isFieldDeadlineValid;
  bool _isFieldStatusValid;

  TextEditingController _controllerTitle = TextEditingController();
  TextEditingController _controllerTask = TextEditingController();
  TextEditingController _controllerDeadline = TextEditingController();
  TextEditingController _controllerStatus = TextEditingController();

  String _statusValue;

  DateTime selectedDate = DateTime.now();

  var customFormat = DateFormat('dd-MM-yyyy');

  Future showPicker() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2018),
        lastDate: DateTime(2101));

    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  void initState() {
    if (widget.tasks != null) {
      _isFieldTitleValid = true;
      _controllerTitle.text = widget.tasks.title;
      _isFieldTitleValid = true;

      _isFieldTaskValid = true;
      _controllerTask.text = widget.tasks.task;
      _isFieldTaskValid = true;

      // _statusValue = widget.tasks.status;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldState,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          title: Text(
            widget.tasks == null ? "Form Add" : "Change Data",
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text("Title"),
                    _buildTextFieldTitle(),
                    Container(
                      width: double.infinity,
                      height: 10,
                    ),
                    Text("Task"),
                    _buildTextFieldTask(),
                    Container(
                      width: double.infinity,
                      height: 10,
                    ),
                    Text("Status"),
                    _buildDropdownStatus(),
                    Container(
                      width: double.infinity,
                      height: 10,
                    ),
                    Text("Deadline"),
                    _buildDatePickerDeadline(),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: RaisedButton(
                        child: Text(
                          widget.tasks == null
                              ? "Submit".toUpperCase()
                              : "Update Data".toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () {
                          if (_isFieldTitleValid == null ||
                              _isFieldTaskValid == null || _statusValue.isEmpty) {
                            _scaffoldState.currentState.showSnackBar(
                              SnackBar(
                                content: Text("Please fill all field"),
                              ),
                            );
                            return;
                          }

                          setState(() => _isLoading = true);
                          String title = _controllerTitle.text.toString();
                          String task = _controllerTask.text.toString();
                          String deadline = customFormat.format(selectedDate);
                          String status = _statusValue.toString();

                          // Profile profile = Profile(name: name, email: email, age: age);

                          Tasks tasks = Tasks(
                              title: title,
                              task: task,
                              deadline: deadline,
                              status: status);

                          if (widget.tasks == null) {
                            _apiServices.createTasks(tasks).then((isSuccess) {
                              setState(() => _isLoading = false);
                              if (isSuccess) {
                                Navigator.pop(
                                    _scaffoldState.currentState.context, true);
                              } else {
                                _scaffoldState.currentState
                                    .showSnackBar(SnackBar(
                                  content: Text("Submit data failed"),
                                ));
                              }
                            });
                          } else {
                            tasks.id = widget.tasks.id;
                            _apiServices.updateTasks(tasks).then((isSuccess) {
                              setState(() => _isLoading = false);
                              if (isSuccess) {
                                Navigator.pop(
                                    _scaffoldState.currentState.context, true);
                              } else {
                                _scaffoldState.currentState
                                    .showSnackBar(SnackBar(
                                  content: Text("Update data failed"),
                                ));
                              }
                            });
                          }
                        },
                        color: Colors.orange[600],
                      ),
                    ),
                  ],
                ),
              ),
              _isLoading
                  ? Stack(
                      children: <Widget>[
                        Opacity(
                          opacity: 0.3,
                          child: ModalBarrier(
                            dismissible: false,
                            color: Colors.grey,
                          ),
                        ),
                        Center(
                          child: CircularProgressIndicator(),
                        ),
                      ],
                    )
                  : Container(),
            ],
          ),
        ));
  }

  Widget _buildTextFieldTitle() {
    return TextField(
      controller: _controllerTitle,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        errorText: _isFieldTitleValid == null || _isFieldTitleValid
            ? null
            : "Title is required",
        border: OutlineInputBorder(),
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldTitleValid) {
          setState(() => _isFieldTitleValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldTask() {
    return TextField(
      controller: _controllerTask,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          errorText: _isFieldTaskValid == null || _isFieldTaskValid
              ? null
              : "Task is required",
          border: OutlineInputBorder()),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldTaskValid) {
          setState(() => _isFieldTaskValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldDeadline() {
    return TextField(
      controller: _controllerDeadline,
      keyboardType: TextInputType.text,
      focusNode: FocusNode(),
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        labelText: "Deadline",
        errorText: _isFieldDeadlineValid == null || _isFieldDeadlineValid
            ? null
            : "Deadline is required",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldDeadlineValid) {
          setState(() => _isFieldDeadlineValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildTextFieldStatus() {
    return TextField(
      controller: _controllerStatus,
      keyboardType: TextInputType.text,
      enableInteractiveSelection: false, // will disable paste operation
      focusNode: FocusNode(),
      decoration: InputDecoration(
        labelText: "Status",
        errorText: _isFieldStatusValid == null || _isFieldStatusValid
            ? null
            : "Status is required",
      ),
      onChanged: (value) {
        bool isFieldValid = value.trim().isNotEmpty;
        if (isFieldValid != _isFieldStatusValid) {
          setState(() => _isFieldStatusValid = isFieldValid);
        }
      },
    );
  }

  Widget _buildDropdownStatus() {
    return Container(
        width: double.infinity,
        height: 60,
        decoration: BoxDecoration(
            border: Border.all(
                color: Colors.grey, width: 1, style: BorderStyle.solid),
            borderRadius: BorderRadius.circular(4)),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
              isExpanded: true,
              value: _statusValue,
              //elevation: 5,
              style: TextStyle(color: Colors.black),
              onChanged: (String value) {
                setState(() {
                  _statusValue = value;
                });
              },
              items: <String>[
                'Inprogress',
                'Done',
                'Cancel',
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(
                    value,
                    style: TextStyle(color: Colors.black),
                  ),
                );
              }).toList(),
              hint: Text(
                "Please choose",
                style: TextStyle(color: Colors.black, fontSize: 14),
              )),
        ));
  }

  _buildDatePickerDeadline() {
    return GestureDetector(
      onTap: showPicker,
      child: Container(
        width: double.infinity,
        height: 60,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            border: Border.all(
                color: Colors.grey, width: 1, style: BorderStyle.solid),
            borderRadius: BorderRadius.circular(4)),
        child: Text(
          '${customFormat.format(selectedDate)}',
        ),
      ),
    );
  }
}
